# README #

## Plugin description ##

This is a test plugin for Wordpress developer position at Inpsyde.

It does the following:
- registers new endpoint on http(s)://domain.tld/testplugin
- calls the test API [endpoint](https://jsonplaceholder.typicode.com/) (and caches result)
- outputs the result on the forementioned page as a table with clickable elements
- when user clicks one of the table`s elements - plugin will retreive (, cache) and output 'user' data in a special field between Page name and Users table as a formatted JSON (at one time maximum one user data will be displayed here)

## Dependencies ##

### Backend ###
 
- main
    - guzzlehttp/guzzle - backend PHP HTTP client
    - gamajo/template-loader - implements Woocommerce-like template override system
- dev
    - phpunit/phpunit - testing framework
    - brain/monkey - unit test utility with Wordpress testing helper functions
    - inpsyde/php-coding-standards - required phpcs extension by the development assignment

### Frontend ###

- Webpack
- SCSS
    - Bootstrap 4 (grid, tables)
- Javascript
    - whatwg-fetch - fetch polyfill for older browsers


## Technologies used and why ##

- Guzzle - author personally loves this one more than WP_Http and WP_Curl and thinks that Guzzle is more robust 
- gamajo/template-loader - (additional) option to override main (and single) plugin template
- PHPUnit and Brain Monkey - because assignment links to better way of testing (without running Wordpress core) [link](https://wordpress.stackexchange.com/questions/164121/testing-hooks-callback/164138#164138) 

## Installation ##

There are several ways to install this plugin:

- via Composer:
    - add plugin repository to composer repositories list
    - add plugin required - `composer require yaros/testplugin`
    - run composer update
- via standart WP installation:
    - download repository as zip file
    - in your WP admin go to Plugins > Add new
    - upload file
    - go to your plugins directory into testplugin directory
    - run composer install there
    - activate plugin in WP admin

## Development decisions explanation ##

#### Endpoint registration way ####

There are 3 main ways to register endpoint:

- REST API endoint (was explicitly declined by the assignment)
- Rewrite rule (will be "recognized by WP as a standard URL")
- "parse_request" hook - author used this one

#### Bootstrap usage ####

Author used this one for faster implementation of responsiveness.

#### No frontend frameworks or jQuery usage ####

At the moment of writing author has minor React.js development experience and cannot provide best quality (but willing to get more experience).

jQuery is not used because author tries to use more native Javascript in development.

#### REST api endpoint registration ####

It would be much more convenient to query external API to fetch single user data (and less development time) in browser, but there would be no server-side HTTP request caching. Frontend HTTP requests caching could be implemented, but assignment asked to focus on the backend. 

#### Transients usage ####

Author used transients to cache request results. One hour cache lifetime is not the best choice and must be reconsidered in real life.

#### Composer ####

Composer usage is mandatory and PSR-4 autoloading is standartised (this cannot be said about Wordpress classes autoloading).

Composer is also used to declare package as WP plugin as stated here [wpackagist](https://wpackagist.org/) and also this implementation is used in Inpsyde Github repositories ([example](https://github.com/inpsyde/paypal-plus-plugin/blob/develop/composer.json)) 

#### Testing implementation ####

At the moment of writing author has only starter knowledge in testing code (truth to be told - author did not have any experience in code testing before this assignment). Author has written and implemented only testing based on the Inpsyde employee article [link](https://wordpress.stackexchange.com/questions/164121/testing-hooks-callback/164138#164138). 

#### Composer support ####

Plugin can work with global composer installation and with local one (inside plugin itself).

#### Template override ####

Plugin supports overriding main (and the only) template - just put new main.php file into your-theme/testplugin directory.

#### Roots Bedrock support ####

Author uses [Roots Bedrock](https://roots.io/bedrock/) on a daily basis. Plugin is developed on a such installation.

#### Webpack implementation ####

If one wishes to update code in here - there are additional steps after installation:
- run `npm install`
- run:
    - `npm run start` to watch files
    - `npm run build` to build for production