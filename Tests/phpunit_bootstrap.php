<?php
/** ---------------------------------------------------- **/
// Require the vendors autoload file.
/** ---------------------------------------------------- **/
require_once dirname(__DIR__, 1) . '/vendor/autoload.php';

define('ABSPATH', dirname(__DIR__, 1));

define( 'MINUTE_IN_SECONDS', 60 );
define( 'HOUR_IN_SECONDS', 60 * MINUTE_IN_SECONDS );