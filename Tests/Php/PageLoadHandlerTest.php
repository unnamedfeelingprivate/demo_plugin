<?php
namespace TestPlugin_Tests;

use TestPlugin\Handlers\PageLoadHandler;
use function Brain\Monkey\Functions\when;

class PageLoadHandlerTest extends MyTestCase {
	public function testGenerateTableHtml(  ) {
		when('set_transient')->justReturn(true);
		when('get_transient')->justReturn('');
		when('maybe_serialize')->justReturn('');
		when('esc_html')->returnArg();
		when('wp_kses')->returnArg();

		$pageHandler = new PageLoadHandler();

		$result = $pageHandler->generateTableHtml();

		$this->assertIsString($result);
		$this->assertTrue(!empty($result));
	}
}