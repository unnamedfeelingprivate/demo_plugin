<?php
namespace TestPlugin_Tests;

use GuzzleHttp\Client;
use TestPlugin\Handlers\ExternalApiHandler as Testee;

class ExternalApiHandlerTest extends MyTestCase {
	public function testClient() {
		$this->assertInstanceOf( Client::class, (new Testee())->client());
	}
}