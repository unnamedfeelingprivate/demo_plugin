<?php
namespace TestPlugin_Tests;

use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;

class MyTestCase extends TestCase
{
	// Adds Mockery expectations to the PHPUnit assertions count.
	use MockeryPHPUnitIntegration;

	protected function setUp(): void
	{
		parent::setUp();
		Monkey\setUp();
	}

	protected function tearDown(): void
	{
		Monkey\tearDown();
		parent::tearDown();
	}
}