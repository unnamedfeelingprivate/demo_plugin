let linkHandlerComponent = new function(){
	let self = this
	
	self.tables = document.querySelectorAll('.js-testPlugin-table')
	
	self.init = () => {
		if (!self.tables) return false;
		
		self.tables.forEach(table => {
			table.querySelectorAll('a').forEach(link => {
				
				link.addEventListener('click', event => {
					event.preventDefault()
					let userId = event.target.closest('tr').dataset.user
					
					fetch('/wp-json/testplugin/v1/getUserById/' + userId)
						.then(response => response.json())
						.then(json => {
							document.querySelector('.js-testPluginConsole').innerText = JSON.stringify(json, null, 2)
						})
				})
			})
		})
	}
};

export default linkHandlerComponent