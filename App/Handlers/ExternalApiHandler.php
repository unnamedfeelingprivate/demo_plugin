<?php
declare(strict_types=1);

namespace TestPlugin\Handlers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class ExternalApiHandler
{
    /**
     * ExternalApiHandler constructor.
     */
    public function __construct()
    {
        if (!defined('ABSPATH')) {
            die();
        }
    }

    /**
     * @return Client
     */
    public function client(): Client
    {
        return new Client([
            'base_uri' => 'https://jsonplaceholder.typicode.com',
            'timeout' => 2.0,
        ]);
    }

    /**
     * @return array
     */
    public function users(): array
    {
        $data = get_transient('unnamed_jsonMockData');

        if (!$data) {
            $requestObject = $this->client()->get('users');

            if (!$this->handleError($requestObject)) {
                return $this->handleError($requestObject);
            }

            return $this->handleResponse($requestObject, 'unnamed_', 'jsonMockData')[0];
        }

        return maybe_unserialize($data);
    }

    /**
     * @param int $id
     * @return array|object
     */
    public function user(int $id): object
    {
        $data = get_transient('unnamed_user_'.$id);

        if (!$data) {
            $requestObject = $this->client()->get('users/'.$id);

            if (!$this->handleError($requestObject)) {
                return $this->handleError($requestObject);
            }

            return $this->handleResponse($requestObject, 'unnamed_user_', (string) $id)[0];
        }

        return maybe_unserialize($data);
    }

    /**
     * @param Response $requestObject
     *
     * @return array|bool
     */
    public function handleError(ResponseInterface $requestObject): array
    {
        if ((string)$requestObject->getStatusCode()[0] !== '2') {
            return ['requestStatus' => 'error', 'message' => $requestObject->getReasonPhrase()];
        }

        return [];
    }

    /**
     * @param Response $requestObject
     * @param string $prefix
     * @param string $suffix
     *
     * @return mixed
     */
    public function handleResponse(
        ResponseInterface $requestObject,
        string $prefix,
        string $suffix = ''
    ): array {

        $data = json_decode($requestObject->getBody()->getContents(), false);
        set_transient($prefix.$suffix, maybe_serialize($data), HOUR_IN_SECONDS);

        return [$data];
    }
}
