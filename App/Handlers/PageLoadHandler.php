<?php
declare(strict_types=1);

namespace TestPlugin\Handlers;

use TestPlugin\Loaders\TemplateLoader;
use WP_Query;

class PageLoadHandler
{
    /**
     * PageLoadHandler constructor.
     */
    public function __construct()
    {
        if (!defined('ABSPATH')) {
            die();
        }
    }

    public function handlePageLoad(): void
    {
        add_filter('template_include', [$this, 'filterContent'], 10);
        add_filter('pre_handle_404', [$this, 'override404'], 10, 2);
    }

    public function filterContent(): void
    {
        $tplLoader = new TemplateLoader();

        $table = ['tableHtml' => $this->generateTableHtml()];

        $tplLoader->set_template_data($table, 'data');
        $tplLoader->get_template_part('main');
    }

    /**
     * @param bool $is404
     * @param WP_Query $wpQuery
     * @return bool
     */
    public function override404(bool $is404, WP_Query $wpQuery): bool
    {
        global $wp_query;

        $wp_query = new WP_Query([]);
        status_header(200);

        return true;
    }

    /**
     * @return string
     */
    public function generateTableHtml(): string
    {
        $apiHandler = new ExternalApiHandler();
        $users = $apiHandler->users();
        
        if (!empty($users)) {
            $html = '<table class="table table-striped testPlugin-table js-testPlugin-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>%s</tbody>
            </table>';
            
            ob_start();
            foreach ($users as $user) {
                $id = !empty($user->id) ? esc_html($user->id) : '';
                $idLink = !empty($user->id) ? sprintf('<a href="#">%s</a>', esc_html($user->id)) : '';
                $nameLink = !empty($user->name) ? sprintf('<a href="#">%s</a>', esc_html($user->name)) : '';
                $usernameLink = !empty($user->username) ?
                    sprintf('<a href="#">%s</a>', esc_html($user->username)) : '';
                $emailLink = !empty($user->email) ? sprintf('<a href="#">%s</a>', esc_html($user->email)) : '';

                $ksesConf = [
                    'a' => [
                        'href' => [],
                        'title' => [],
                    ],
                ];

                printf(
                    '<tr data-user="%s"><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                    wp_kses($id, $ksesConf),
                    wp_kses($idLink, $ksesConf),
                    wp_kses($nameLink, $ksesConf),
                    wp_kses($usernameLink, $ksesConf),
                    wp_kses($emailLink, $ksesConf)
                );
            }
            return sprintf($html, ob_get_clean()); // WPCS: XSS OK.
        }

        return '';
    }
}
