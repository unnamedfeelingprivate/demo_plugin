<?php
declare(strict_types=1);

namespace TestPlugin\Handlers;

use WP_REST_Request;

class APIHandler
{
    /**
     * APIHandler constructor.
     */
    public function __construct()
    {
        if (!defined('ABSPATH')) {
            die();
        }

        $this->registerEndpoints();
    }

    /**
     * Register REST API endpoints
     */
    private function registerEndpoints(): void
    {
        $params = [
            'GET' => [
                [
                    'route' => '/getUserById/(?P<id>\d+)',
                    'callback' => 'fetchUserById',
                ],
            ],
        ];

        foreach ($params['GET'] as $endpoint) {
            if (empty($endpoint['route'])||empty($endpoint['callback'])) {
                continue;
            }

            register_rest_route(self::returnBase(), $endpoint['route'], [
                'methods' => 'GET',
                'callback' => [$this, $endpoint['callback']],
            ]);
        }
    }

    /**
     * @param WP_REST_Request $request
     */
    public static function fetchUserById(WP_REST_Request $request): void
    {
        $data = $request->get_params();
        $handler = new ExternalApiHandler();
        $result = $handler->user((int) $data['id']);

        wp_send_json($result);
    }

    /**
     * @return string
     */
    public static function returnBase(): string
    {
        return 'testplugin/v1';
    }
}
